import { TestBed } from '@angular/core/testing';

import { SessionControlService } from './session-control.service';

describe('SessionControlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SessionControlService = TestBed.get(SessionControlService);
    expect(service).toBeTruthy();
  });
});
