import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionControlService {

  private _running: boolean = true;

  constructor() {}

  set running(value: boolean) {
    this._running = value;
  }

  get running(): boolean {
    return this._running;
  }
}
