export class ImuMeasurement {
  private readonly _time: Date;
  private readonly _deviceId: number;
  private readonly _sessionId: number;
  private readonly _imu1Lx: number;
  private readonly _imu1Ly: number;
  private readonly _imu1Lz: number;
  private readonly _imu1LDeviation: number;
  private readonly _imu1LDeviationColor: string;
  private readonly _imu1Rx: number;
  private readonly _imu1Ry: number;
  private readonly _imu1Rz: number;
  private readonly _imu1RDeviation: number;
  private readonly _imu1RDeviationColor: string;
  private readonly _imu2Lx: number;
  private readonly _imu2Ly: number;
  private readonly _imu2Lz: number;
  private readonly _imu2LDeviation: number;
  private readonly _imu2LDeviationColor: string;
  private readonly _imu2Rx: number;
  private readonly _imu2Ry: number;
  private readonly _imu2Rz: number;
  private readonly _imu2RDeviation: number;
  private readonly _imu2RDeviationColor: string;
  private readonly _imu3Lx: number;
  private readonly _imu3Ly: number;
  private readonly _imu3Lz: number;
  private readonly _imu3LDeviation: number;
  private readonly _imu3LDeviationColor: string;
  private readonly _imu3Rx: number;
  private readonly _imu3Ry: number;
  private readonly _imu3Rz: number;
  private readonly _imu3RDeviation: number;
  private readonly _imu3RDeviationColor: string;
  private readonly _imu4Lx: number;
  private readonly _imu4Ly: number;
  private readonly _imu4Lz: number;
  private readonly _imu4LDeviation: number;
  private readonly _imu4LDeviationColor: string;
  private readonly _imu4Rx: number;
  private readonly _imu4Ry: number;
  private readonly _imu4Rz: number;
  private readonly _imu4RDeviation: number;
  private readonly _imu4RDeviationColor: string;
  private readonly _imuDeviation: number;


  constructor(time: Date, deviceId: number, sessionId: number,
              imu1Lx: number, imu1Ly: number, imu1Lz: number,
              imu1Rx: number, imu1Ry: number, imu1Rz: number,
              imu2Lx: number, imu2Ly: number, imu2Lz: number,
              imu2Rx: number, imu2Ry: number, imu2Rz: number,
              imu3Lx: number, imu3Ly: number, imu3Lz: number,
              imu3Rx: number, imu3Ry: number, imu3Rz: number,
              imu4Lx: number, imu4Ly: number, imu4Lz: number,
              imu4Rx: number, imu4Ry: number, imu4Rz: number) {
    this._time = time;
    this._deviceId = deviceId;
    this._sessionId = sessionId;
    this._imu1Lx = imu1Lx;
    this._imu1Ly = imu1Ly;
    this._imu1Lz = imu1Lz;
    this._imu1LDeviation = Math.abs(Math.round((Math.abs(imu1Lx) + Math.abs(imu1Ly) + Math.abs(imu1Lz)) / 3));
    this._imu1LDeviationColor = ImuMeasurement.getColor(this._imu1LDeviation);
    this._imu1Rx = imu1Rx;
    this._imu1Ry = imu1Ry;
    this._imu1Rz = imu1Rz;
    this._imu1RDeviation = Math.abs(Math.round((Math.abs(imu1Rx) + Math.abs(imu1Ry) + Math.abs(imu1Rz)) / 3));
    this._imu1RDeviationColor = ImuMeasurement.getColor(this._imu1RDeviation);
    this._imu2Lx = imu2Lx;
    this._imu2Ly = imu2Ly;
    this._imu2Lz = imu2Lz;
    this._imu2LDeviation = Math.abs(Math.round((Math.abs(imu2Lx) + Math.abs(imu2Ly) + Math.abs(imu2Lz)) / 3));
    this._imu2LDeviationColor = ImuMeasurement.getColor(this._imu2LDeviation);
    this._imu2Rx = imu2Rx;
    this._imu2Ry = imu2Ry;
    this._imu2Rz = imu2Rz;
    this._imu2RDeviation = Math.abs(Math.round((Math.abs(imu2Rx) + Math.abs(imu2Ry) + Math.abs(imu2Rz)) / 3));
    this._imu2RDeviationColor = ImuMeasurement.getColor(this._imu2RDeviation);
    this._imu3Lx = imu3Lx;
    this._imu3Ly = imu3Ly;
    this._imu3Lz = imu3Lz;
    this._imu3LDeviation = Math.abs(Math.round((Math.abs(imu3Lx) + Math.abs(imu3Ly) + Math.abs(imu3Lz)) / 3));
    this._imu3LDeviationColor = ImuMeasurement.getColor(this._imu3LDeviation);
    this._imu3Rx = imu3Rx;
    this._imu3Ry = imu3Ry;
    this._imu3Rz = imu3Rz;
    this._imu3RDeviation = Math.abs(Math.round((Math.abs(imu3Rx) + Math.abs(imu3Ry) + Math.abs(imu3Rz)) / 3));
    this._imu3RDeviationColor = ImuMeasurement.getColor(this._imu3RDeviation);
    this._imu4Lx = imu4Lx;
    this._imu4Ly = imu4Ly;
    this._imu4Lz = imu4Lz;
    this._imu4LDeviation = Math.abs(Math.round((Math.abs(imu4Lx) + Math.abs(imu4Ly) + Math.abs(imu4Lz)) / 3));
    this._imu4LDeviationColor = ImuMeasurement.getColor(this._imu4LDeviation);
    this._imu4Rx = imu4Rx;
    this._imu4Ry = imu4Ry;
    this._imu4Rz = imu4Rz;
    this._imu4RDeviation = Math.abs(Math.round((Math.abs(imu4Rx) + Math.abs(imu4Ry) + Math.abs(imu4Rz)) / 3));
    this._imu4RDeviationColor = ImuMeasurement.getColor(this._imu4RDeviation);
    this._imuDeviation = Math.round((this.imu1LDeviation + this.imu1RDeviation +
      this.imu2LDeviation + this.imu2RDeviation +
      this.imu3LDeviation + this.imu3RDeviation +
      this.imu4LDeviation + this.imu4RDeviation) / 8);
  }

  static getColor(deviation: number): string {
    if (0 <= deviation && deviation < 6) { return '#2e7d32'}
    if (6 <= deviation && deviation < 12) { return '#558b2f'}
    if (12 <= deviation && deviation < 24) { return '#9e9d24'}
    if (24 <= deviation && deviation < 36) { return '#f9a825'}
    if (36 <= deviation && deviation < 54) { return '#ff8f00'}
    if (54 <= deviation && deviation < 72) { return '#ef6c00'}
    if (72 <= deviation && deviation <= 90) { return '#d84315'}
  }

  get imuDeviation(): number {
    return this._imuDeviation;
  }

  get imu1LDeviationColor(): string {
    return this._imu1LDeviationColor;
  }

  get imu1RDeviationColor(): string {
    return this._imu1RDeviationColor;
  }

  get imu2LDeviationColor(): string {
    return this._imu2LDeviationColor;
  }

  get imu2RDeviationColor(): string {
    return this._imu2RDeviationColor;
  }

  get imu3LDeviationColor(): string {
    return this._imu3LDeviationColor;
  }

  get imu3RDeviationColor(): string {
    return this._imu3RDeviationColor;
  }

  get imu4LDeviationColor(): string {
    return this._imu4LDeviationColor;
  }

  get imu4RDeviationColor(): string {
    return this._imu4RDeviationColor;
  }

  get imu1LDeviation(): number {
    return this._imu1LDeviation;
  }

  get imu1RDeviation(): number {
    return this._imu1RDeviation;
  }

  get imu2LDeviation(): number {
    return this._imu2LDeviation;
  }

  get imu2RDeviation(): number {
    return this._imu2RDeviation;
  }

  get imu3LDeviation(): number {
    return this._imu3LDeviation;
  }

  get imu3RDeviation(): number {
    return this._imu3RDeviation;
  }

  get imu4LDeviation(): number {
    return this._imu4LDeviation;
  }

  get imu4RDeviation(): number {
    return this._imu4RDeviation;
  }

  get time(): Date {
    return this._time;
  }

  get deviceId(): number {
    return this._deviceId;
  }

  get sessionId(): number {
    return this._sessionId;
  }

  get imu1Lx(): number {
    return this._imu1Lx;
  }

  get imu1Ly(): number {
    return this._imu1Ly;
  }

  get imu1Lz(): number {
    return this._imu1Lz;
  }

  get imu1Rx(): number {
    return this._imu1Rx;
  }

  get imu1Ry(): number {
    return this._imu1Ry;
  }

  get imu1Rz(): number {
    return this._imu1Rz;
  }

  get imu2Lx(): number {
    return this._imu2Lx;
  }

  get imu2Ly(): number {
    return this._imu2Ly;
  }

  get imu2Lz(): number {
    return this._imu2Lz;
  }

  get imu2Rx(): number {
    return this._imu2Rx;
  }

  get imu2Ry(): number {
    return this._imu2Ry;
  }

  get imu2Rz(): number {
    return this._imu2Rz;
  }

  get imu3Lx(): number {
    return this._imu3Lx;
  }

  get imu3Ly(): number {
    return this._imu3Ly;
  }

  get imu3Lz(): number {
    return this._imu3Lz;
  }

  get imu3Rx(): number {
    return this._imu3Rx;
  }

  get imu3Ry(): number {
    return this._imu3Ry;
  }

  get imu3Rz(): number {
    return this._imu3Rz;
  }

  get imu4Lx(): number {
    return this._imu4Lx;
  }

  get imu4Ly(): number {
    return this._imu4Ly;
  }

  get imu4Lz(): number {
    return this._imu4Lz;
  }

  get imu4Rx(): number {
    return this._imu4Rx;
  }

  get imu4Ry(): number {
    return this._imu4Ry;
  }

  get imu4Rz(): number {
    return this._imu4Rz;
  }

  public static fromJson(json: any): ImuMeasurement {
    return new ImuMeasurement(
      new Date(json['time']),
      json['deviceId'],
      json['sessionId'],
      json['imu1Lx'],
      json['imu1Ly'],
      json['imu1Lz'],
      json['imu1Rx'],
      json['imu1Ry'],
      json['imu1Rz'],
      json['imu2Lx'],
      json['imu2Ly'],
      json['imu2Lz'],
      json['imu2Rx'],
      json['imu2Ry'],
      json['imu2Rz'],
      json['imu3Lx'],
      json['imu3Ly'],
      json['imu3Lz'],
      json['imu3Rx'],
      json['imu3Ry'],
      json['imu3Rz'],
      json['imu4Lx'],
      json['imu4Ly'],
      json['imu4Lz'],
      json['imu4Rx'],
      json['imu4Ry'],
      json['imu4Rz'],
    )
  }
}
