export class SessionResponse {
  private readonly _sessionId: number;

  constructor(sessionId: number) {
    this._sessionId = sessionId;
  }

  get sessionId(): number {
    return this._sessionId;
  }

  public static fromJson(json: any): SessionResponse {
    return new SessionResponse(json['sessionId']);
  }
}
