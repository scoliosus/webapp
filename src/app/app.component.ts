import {Component, ViewChild} from '@angular/core';
import {SidenavComponent} from "./component/sidenav/sidenav.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild(SidenavComponent) sidenav: SidenavComponent;
}
