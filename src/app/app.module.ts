import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BasicTimeSeriesComponent} from "./component/basic-time-series/basic-time-series.component";
import {SidenavComponent} from './component/sidenav/sidenav.component';
import {MaterialModule} from "./modules/material.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import { TimeSeriesConfigComponent } from './component/time-series-config/time-series-config.component';
import { ExportComponent } from './component/export/export.component';
import {MatBottomSheetModule, MatNativeDateModule} from "@angular/material";
import {FlexLayoutModule} from "@angular/flex-layout";
import { AllSessionsComponent } from './component/all-sessions/all-sessions.component';
import { SessionControlComponent } from './component/session-control/session-control.component';
import {FormsModule} from "@angular/forms";
import { HistogramComponent } from './component/histogram/histogram.component';

@NgModule({
  declarations: [
    AppComponent,
    BasicTimeSeriesComponent,
    SidenavComponent,
    TimeSeriesConfigComponent,
    ExportComponent,
    AllSessionsComponent,
    SessionControlComponent,
    HistogramComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatNativeDateModule,
    FlexLayoutModule,
    MaterialModule,
    BrowserAnimationsModule,
    MatBottomSheetModule,
    HttpClientModule
  ],
  entryComponents: [ExportComponent, SessionControlComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
