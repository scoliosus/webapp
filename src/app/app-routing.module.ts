import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TimeSeriesConfigComponent} from "./component/time-series-config/time-series-config.component";
import {SessionControlComponent} from "./component/session-control/session-control.component";
import {AllSessionsComponent} from "./component/all-sessions/all-sessions.component";

const routes: Routes = [
  {
    path:      '',
    component: TimeSeriesConfigComponent
  }, {
    path:      'sessions',
    component: AllSessionsComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
