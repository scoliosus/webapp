import {ImuMeasurement} from "../model/imu-measurement";

export class GeneratePlotConfig {

  public static generate1LConfig(measurements: ImuMeasurement[]): any[] {
    return [
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu1Lx),
        name: 'X Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu1Ly),
        name: 'Y Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu1Lz),
        name: 'Z Axis'
      }
    ];
  }

  public static generate1RConfig(measurements: ImuMeasurement[]): any[] {
    return [
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu1Rx),
        name: 'X Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu1Ry),
        name: 'Y Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu1Rz),
        name: 'Z Axis'
      }
    ];
  }

  public static generate2LConfig(measurements: ImuMeasurement[]): any[] {
    return [
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu2Lx),
        name: 'X Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu2Ly),
        name: 'Y Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu2Lz),
        name: 'Z Axis'
      }
    ];
  }

  public static generate2RConfig(measurements: ImuMeasurement[]): any[] {
    return [
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu2Rx),
        name: 'X Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu2Ry),
        name: 'Y Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu2Rz),
        name: 'Z Axis'
      }
    ];
  }

  public static generate3LConfig(measurements: ImuMeasurement[]): any[] {
    return [
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu3Lx),
        name: 'X Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu3Ly),
        name: 'Y Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu3Lz),
        name: 'Z Axis'
      }
    ];
  }

  public static generate3RConfig(measurements: ImuMeasurement[]): any[] {
    return [
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu3Rx),
        name: 'X Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu3Ry),
        name: 'Y Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu3Rz),
        name: 'Z Axis'
      }
    ];
  }

  public static generate4LConfig(measurements: ImuMeasurement[]): any[] {
    return [
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu4Lx),
        name: 'X Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu4Ly),
        name: 'Y Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu4Lz),
        name: 'Z Axis'
      }
    ];
  }

  public static generate4RConfig(measurements: ImuMeasurement[]): any[] {
    return [
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu4Rx),
        name: 'X Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu4Ry),
        name: 'Y Axis'
      },
      {
        x: measurements.map(measurement => measurement.time),
        y: measurements.map(measurement => measurement.imu4Rz),
        name: 'Z Axis'
      }
    ];
  }
}
