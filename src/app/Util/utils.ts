import {formatDate} from '@angular/common';

/**
 * Random util functions.
 * @author Lucas Gauk
 */
export class Utils {

  /**
   * Return a correctly formatted string form a date.
   * @param date
   * @param format
   */
  static dateToString(date: Date, format = 'yyyy-MM-dd') {
    if (date) {
      return formatDate(date, format, 'en-US');
    }
    return null;
  }

  /**
   * Test to see if a string matches the expected yyyy-MM-dd format.
   * @param date
   */
  static checkDateString(date: string) {
    const regex = /^\d{4}-\d{2}-\d{2}$/;
    return regex.test(date);
  }

  static randomColor(numOfSteps: number, step: number): string {
    // This function generates vibrant, 'evenly spaced" colours (i.e. no clustering).
    // This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
    // Adam Cole, 2011-Sept-14
    // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
    /* tslint:disable:no-bitwise */

    let r, g, b;
    const h = step / numOfSteps;
    const i = ~~(h * 6);
    const f = h * 6 - i;
    const q = 1 - f;
    switch (i % 6) {
      case 0: r = 1; g = f; b = 0; break;
      case 1: r = q; g = 1; b = 0; break;
      case 2: r = 0; g = 1; b = f; break;
      case 3: r = 0; g = q; b = 1; break;
      case 4: r = f; g = 0; b = 1; break;
      case 5: r = 1; g = 0; b = q; break;
    }
    const c = '#' + ('00' + (~ ~(r * 255)).toString(16)).slice(-2) + ('00' + (~ ~(g * 255))
      .toString(16)).slice(-2) + ('00' + (~ ~(b * 255)).toString(16)).slice(-2);

    /* tslint:enable:no-bitwise */
    return c;
  }

  static dateDifference(date1: Date, date2: Date): number {
    date1 = new Date(date1);
    date2 = new Date(date2);
    return Math.ceil(Math.abs(date2.getTime() - date1.getTime()) / (1000 * 3600 * 24));
  }

  static addDays(date: Date, days: number): Date {
    const result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

}
