import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from "@angular/material";
import {SessionControlService} from "../../service/session-control.service";

@Component({
  selector: 'app-session-control',
  templateUrl: './session-control.component.html',
  styleUrls: ['./session-control.component.css']
})
export class SessionControlComponent implements OnInit {

  currentSession: number;
  running: boolean;

  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
              private controlService: SessionControlService,
              private bottomSheetRef: MatBottomSheetRef<SessionControlComponent>) {
    this.currentSession = data['sessionId'];
  }

  ngOnInit() {
    this.running = this.controlService.running;
  }

  toggleRunning(running: boolean) {
    this.controlService.running = running;
    this.bottomSheetRef.dismiss();
  }

}
