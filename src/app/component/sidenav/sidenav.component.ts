import {Component, ViewChild} from '@angular/core';
import {MatSidenav} from "@angular/material";

@Component({
  selector:    'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls:   ['./sidenav.component.css']
})

/**
 * Responsive side nav. Hovers over content if the width of the screen becomes
 * too small. Otherwise it slides it out of the way.
 * @author Lucas Gauk
 */
export class SidenavComponent {
  @ViewChild('sideNav') sideNav: MatSidenav;

  constructor() {
  }

  toggleSidenav() {
    this.sideNav.toggle();
  }


}

