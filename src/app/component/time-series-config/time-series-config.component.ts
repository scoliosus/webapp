import {Component, OnDestroy, OnInit, ViewChild, ViewChildren} from '@angular/core';
import {BasicTimeSeriesComponent} from "../basic-time-series/basic-time-series.component";
import {ImuMeasurement} from "../../model/imu-measurement";
import {HttpService} from "../../service/http.service";
import {SessionControlComponent} from "../session-control/session-control.component";
import {MatBottomSheet} from "@angular/material";
import {SessionControlService} from "../../service/session-control.service";
import {GeneratePlotConfig} from "../../Util/generate-plot-config";
import {HistogramComponent} from "../histogram/histogram.component";
import {reference} from "@angular/core/src/render3";
import {interval} from "rxjs";
import {startWith, switchMap} from "rxjs/operators";

@Component({
  selector: 'app-time-series-config',
  templateUrl: './time-series-config.component.html',
  styleUrls: ['./time-series-config.component.css']
})
export class TimeSeriesConfigComponent implements OnInit, OnDestroy {

  @ViewChildren(BasicTimeSeriesComponent) imuPlots: BasicTimeSeriesComponent[];
  @ViewChild(HistogramComponent) histogram: HistogramComponent;

  running: boolean = true; // True if we are currently recording points.
  displayMeasurements: ImuMeasurement[] = []; // Measurements currently spread throughout the graphs.
  histogramXValues: number[] = []; // X Values we pass to the histogram.
  averageDeviation: string; // Our calculated average deviation based on the histogram x values.
  onTarget = 10; // Considered on target if the average deviation is less than onTarget.
  pOnTarget: string; // Ratio of points on target vs points off.
  hover = 1; // Which body part is being hovered over. Defaults to top left.
  httpInterval: any; // Interval handle for the http requests.
  stopWatchInterval: any; // Interval handle for the stopwatch.
  runningSeconds = 0; // Running seconds, repeats at 60.
  runningMinutes = 0; // Running minutes, never repeats (needs to be fixed).
  loading = false; // True if we're waiting on the last HTTP response.
  sessionId; // Current session.

  constructor(private httpService: HttpService,
              private bottomSheet: MatBottomSheet,
              private controlService: SessionControlService) {
  }

  /**
   * Start both intervals on init.
   */
  ngOnInit() {
    this.running = this.controlService.running;
    this.httpInterval = setInterval(() => {
      if (this.running && !this.loading) {
        this.getSeries();
      }
    }, 500);

    this.stopWatchInterval = setInterval(() => {
      if (this.running) {
        this.runningSeconds += 1;
        if (this.runningSeconds >= 60) {
          this.runningMinutes += 1;
          this.runningSeconds = 0;
        }
      }
    }, 1000);
  }

  /**
   * Clear our intervals on destroy.
   */
  ngOnDestroy(): void {
    clearInterval(this.httpInterval);
    clearInterval(this.stopWatchInterval);
    this.histogramXValues = [];
    this.displayMeasurements = [];
  }

  /**
   * Request points from the last 2 seconds from the API.
   * Save them to our display points, but delete duplicates.
   */
  getSeries() {
    const qp = {};
    qp['milliseconds'] = 5000;
    this.loading = true;
    const reference = this.httpService.get(qp, 'influx/milliseconds').subscribe(response => {
      reference.unsubscribe();
      for (const item of response) {
        // Check for duplicates and add to displayMeasurements.
        let newMeasurement = ImuMeasurement.fromJson(item);
        if (this.displayMeasurements.find(measurement => measurement.time.getTime() == newMeasurement.time.getTime()) == null) {
          this.displayMeasurements.push(newMeasurement);
          this.histogramXValues.push(newMeasurement.imuDeviation);
        }
        // Shift out our display measurements if we have more than 10 points.
        if (this.displayMeasurements.length >= 40) {
          this.displayMeasurements.shift();
        }
        // Shift out our x values eventually.
        if (this.histogramXValues.length >= 200) {
          this.histogramXValues.shift();
        }
      }
      // If the session ID changes reset our values.
      if (this.displayMeasurements.length > 0) {
        if (this.sessionId != this.displayMeasurements[this.displayMeasurements.length - 1].sessionId) {
          this.sessionId = this.displayMeasurements[this.displayMeasurements.length - 1].sessionId;
          this.displayMeasurements = [];
          this.histogramXValues = [];
          this.runningMinutes = this.runningSeconds = 0;
        }
      }
      // Calculate average deviation and percentage on target from histogram x values.
      if (this.histogramXValues.length > 0) {
        this.averageDeviation = (this.histogramXValues.reduce((a, b) => a + b) / this.histogramXValues.length)
          .toFixed(2);
        const underTarget = (100 * this.histogramXValues.filter(x => x < this.onTarget).length);
        this.pOnTarget = (underTarget / this.histogramXValues.length).toFixed(2);
      }
      this.displayTraces();
    });
  }

  /**
   * Get each component to display the correct traces.
   */
  displayTraces() {
    this.imuPlots.find((template, index) => index == 0).displayTraces(GeneratePlotConfig.generate1LConfig(this.displayMeasurements));
    this.imuPlots.find((template, index) => index == 1).displayTraces(GeneratePlotConfig.generate1RConfig(this.displayMeasurements));
    this.imuPlots.find((template, index) => index == 2).displayTraces(GeneratePlotConfig.generate2LConfig(this.displayMeasurements));
    this.imuPlots.find((template, index) => index == 3).displayTraces(GeneratePlotConfig.generate2RConfig(this.displayMeasurements));
    this.imuPlots.find((template, index) => index == 4).displayTraces(GeneratePlotConfig.generate3LConfig(this.displayMeasurements));
    this.imuPlots.find((template, index) => index == 5).displayTraces(GeneratePlotConfig.generate3RConfig(this.displayMeasurements));
    this.imuPlots.find((template, index) => index == 6).displayTraces(GeneratePlotConfig.generate4LConfig(this.displayMeasurements));
    this.imuPlots.find((template, index) => index == 7).displayTraces(GeneratePlotConfig.generate4RConfig(this.displayMeasurements));
    this.histogram.displayTraces();
    this.loading = false;
  }

  /**
   * Handle opening the bottom sheet to view export options.
   */
  public openBottomSheet() {
    const ref = this.bottomSheet.open(SessionControlComponent, { data: { sessionId: this.sessionId }});
    ref.afterDismissed().subscribe(() => this.bottomSheetClosed());
  }

  /**
   * On bottom sheet closing check if we should pause.
   */
  bottomSheetClosed() {
    this.running = this.controlService.running;
  }

}
