import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeSeriesConfigComponent } from './time-series-config.component';

describe('TimeSeriesConfigComponent', () => {
  let component: TimeSeriesConfigComponent;
  let fixture: ComponentFixture<TimeSeriesConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeSeriesConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeSeriesConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
