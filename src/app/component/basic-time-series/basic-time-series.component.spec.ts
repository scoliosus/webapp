import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicTimeSeriesComponent } from './basic-time-series.component';

describe('BasicTimeSeriesComponent', () => {
  let component: BasicTimeSeriesComponent;
  let fixture: ComponentFixture<BasicTimeSeriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicTimeSeriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicTimeSeriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
