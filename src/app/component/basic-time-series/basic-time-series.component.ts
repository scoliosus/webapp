import {Component} from '@angular/core';
import * as Plotly from 'plotly.js';
import {Layout} from 'plotly.js';

@Component({
  selector: 'app-basic-time-series',
  templateUrl: './basic-time-series.component.html',
  styleUrls: ['./basic-time-series.component.css']
})

/**
 * Handles getting and displaying a plot based on the TimeSeriesConfig.
 */
export class BasicTimeSeriesComponent {

  plotDivName = 'plot' + Math.floor(Math.random() * 600000); // Random div name in case component is reused.
  yValues: number[];

  constructor() {}

  /**
   * Display InfluxResponse as traces on a plot.
   */
  public displayTraces(values: { x: any[], y: number[], name: string}[]) {
    // Loop through queries and display them on the plot.
    let traces = [];
    this.yValues = [];
    for (let i = 0; i < values.length; i++) {
      this.yValues.push(...values[i]['y']);
      traces.push({
        type: 'scatter',
        name: values[i]['name'],
        x: values[i]['x'],
        y: values[i]['y'],
        fill: 'tonexty',
        line: {
          shape: 'spline',
          smoothing: 1.3
        },
      });
      Plotly.newPlot(this.plotDivName, traces, this.createLayout());
    }
  }

  /**
   * Create Layout element for plot.
   */
  createLayout(): Partial<Layout> {
    const container = document.getElementById('graphContainer');
    const min = Math.min(...this.yValues) - 15;
    const max = Math.max(...this.yValues) + 15;
    return { width: container.offsetWidth - 20,
      xaxis: {'showticklabels': false, 'zeroline': false},
      yaxis:  {'title': 'Degrees', 'range': [min > -10 ? -10 : min, max < 10 ? 10 : max], 'nticks': 18, 'zeroline': true, 'zerolinecolor': '#7CFC00'},
      height: 274, margin: {
        l: 40,
        r: 40,
        b: 40,
        t: 40
      } };
  }

  /**
   * Call relayout on the current plot.
   */
  onResize() {
    if (document.getElementById(this.plotDivName)) {
      Plotly.relayout(this.plotDivName, this.createLayout());
    }
  }

}
