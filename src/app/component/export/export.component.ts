import {Component, Input, OnInit} from '@angular/core';
import {MatBottomSheetRef} from "@angular/material";

@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.css']
})
export class ExportComponent implements OnInit {

  constructor(private bottomSheetRef: MatBottomSheetRef<ExportComponent>) { }

  ngOnInit() {
  }

  /**
   * Handle exporting to CSV. Uses services and passes name of download and the ID
   * of the button we are going to use.
   */
  public exportCsv() {
    // TODO: Add export service to handle CSV export and PDF export.
    this.bottomSheetRef.dismiss();
  }

}
