import {Component, Input} from '@angular/core';
import * as Plotly from 'plotly.js';
import {Layout, PlotData} from 'plotly.js';

@Component({
  selector: 'app-histogram',
  templateUrl: './histogram.component.html',
  styleUrls: ['./histogram.component.css']
})
export class HistogramComponent {

  @Input() xPoints: number[];
  @Input() config: {};
  plotDivName = 'plot' + Math.floor(Math.random() * 600000); // Random div name in case component is reused.

  constructor() { }

  public displayTraces() {
    console.log(this.xPoints);
    const data: Partial<PlotData>[] = [
      {
        x: this.xPoints,
        xbins: {
          end: 90,
          size: 1,
          start: 0
        },
        type: 'histogram',
        marker: {
          color: 'pink',
        },
      }
    ];
    Plotly.newPlot(this.plotDivName, data, this.createLayout());
  }

  /**
   * Create Layout element for plot.
   */
  createLayout(): Partial<Layout> {
    const container = document.getElementById('graphContainer');
    return { width: container.offsetWidth - 20,
      title: 'Session ' + this.config['sessionId'] +  ' Back Deviation Averages',
      xaxis: {title: 'Average Back Deviation'},
      yaxis: {title: 'Count'},
      height: 200, margin: {
        l: 40,
        r: 40,
        b: 40,
        t: 40
      } };
  }

  /**
   * Call relayout on the current plot.
   */
  onResize() {
    if (document.getElementById(this.plotDivName)) {
      Plotly.relayout(this.plotDivName, this.createLayout());
    }
  }

}
