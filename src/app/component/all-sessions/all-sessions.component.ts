import {Component, OnInit} from '@angular/core';
import {MatBottomSheet} from "@angular/material";
import {SessionControlComponent} from "../session-control/session-control.component";
import {HttpService} from "../../service/http.service";
import {SessionResponse} from "../../model/session-response";
import {ImuMeasurement} from "../../model/imu-measurement";

@Component({
  selector: 'app-all-sessions',
  templateUrl: './all-sessions.component.html',
  styleUrls: ['./all-sessions.component.css']
})
export class AllSessionsComponent implements OnInit {

  sessionStart: Date;
  sessionEnd: Date;
  availableSessions: number[];
  selectedSessionId: number;
  measurements: ImuMeasurement[] = [];
  loading = false;


  constructor(private bottomSheet: MatBottomSheet, private httpService: HttpService) { }

  ngOnInit() {
    this.httpService.get({}, 'influx/sessions').subscribe(response => {
      this.availableSessions = [];
      for (const item of response) {
        this.availableSessions.push(SessionResponse.fromJson(item).sessionId);
      }
    });
  }

  dateChange() {
    if (this.sessionStart && this.sessionEnd && this.selectedSessionId) {
      const qp = {};
      qp['startDate'] = this.sessionStart.toISOString().substring(0, 10);
      qp['endDate'] = this.sessionEnd.toISOString().substring(0, 10);
    }
  }

  onSubmit() {
    let endpoint = '/influx/sessions/' + this.selectedSessionId;
    let qp = {};
    qp['startDate'] = this.sessionStart.toISOString().substring(0, 10);
    qp['endDate'] = this.sessionEnd.toISOString().substring(0, 10);
    this.loading = true;
    this.httpService.get(qp, endpoint).subscribe(response => {
      this.loading = false;
      this.measurements = [];
      for (const item of response) {
        this.measurements.push(ImuMeasurement.fromJson(item));
      }
    });
  }

  getAverage(measurement: ImuMeasurement) {
    return ((measurement.imu1LDeviation + measurement.imu1RDeviation +
      measurement.imu2LDeviation + measurement.imu2RDeviation +
      measurement.imu3LDeviation + measurement.imu3RDeviation +
      measurement.imu4LDeviation + measurement.imu4RDeviation) / 8).toFixed(2);
  }

  getColor(value: number) {
    return ImuMeasurement.getColor(value);
  }

  /**
   * Handle opening the bottom sheet to view export options.
   */
  public openBottomSheet() {
    this.bottomSheet.open(SessionControlComponent);
  }

}
